#!/bin/bash
set -e

LPP_BUILD_MODE="release"

source ./conf.sh

lpp3-check ./source/romfs/index.lua

bannertool makesmdh --shorttitle "$PROJECT_NAME" --longtitle "$PROJECT_NAME" --publisher "$PROJECT_PUBLISHER" --icon "./source/metadata/icon.png" --output "./outputs/smdh.bin"
bannertool makebanner --image "./source/metadata/banner.png" --audio "./source/metadata/tune.ogg" --output "./outputs/banner.bin"
lpp3-pack romfs $PROJECT_ID --icon "./outputs/smdh.bin" --banner "./outputs/banner.bin" --3ds-output "./outputs/release_build.3ds" --cia-output "./outputs/release_build.cia" --romfs "./outputs/romfs" --release