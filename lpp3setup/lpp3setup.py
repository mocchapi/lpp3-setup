#!/usr/bin/env python3

import os
import sys
import stat
import shutil
import random
import argparse
from pathlib import Path

ROOT = Path(os.path.realpath(__file__)).parent
CWD =  Path(os.getcwd())

__VERSION__ = '1.0.0'
__SOURCE__  = 'https://gitlab.com/mocchapi/lpp3-setup'


CONF_TEMPLATE = """#!/bin/bash
# made with lpp3-setup version <<<VERSION>>>
# <<<SOURCE>>>


# User-replacable values
PROJECT_NAME="<<<PROJECT_NAME>>>"
PROJECT_ID="<<<PROJECT_ID>>>"
PROJECT_PUBLISHER="<<<PROJECT_PUBLISHER>>>"
"""
OPENMUZE_CONF_ADDITIONS = """

# openmuze compile step
openmuze ./source/code/main.lua ./source/romfs/index.lua --define "buildmode":"'$LPP_BUILD_MODE'"
"""


EXAMPLE_MAIN_LUA = """
-- Put your openmuze compatible LUA code here
-- The name main.lua is important, dont change it
-- Don't put custom code in source/romfs/index.lua, it will get overwritten when building
-- Don't forget to use double {{ }} where you would normally use singles

-- el gato: {"meow "*20}"""

EXAMPLE_INDEX_LUA = """
-- The name index.lua is important, dont change it
-- Your game code lives here
-- If you use openmuze, put your code in source/code/main.lua instead
"""


parser = argparse.ArgumentParser(
	prog="lpp3-setup",
	description="a utility for setting up Luna Player Plus 3DS projects that use `lpp3-pack` quickly",
	epilog="this software is licensed under the AGPLv3.0, for more info see the LICENSE file",
	formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)
parser.add_argument(
    'name', type=str, help="Name of the project"
)
parser.add_argument(
    '--destination', '-d', type=Path, default=None, help="Path to create the project at. Defaults to `./<project name>`"
)
parser.add_argument(
    '--unique-id','-id', type=str, default=f'0x{random.randrange(0,65535)}', help="The UniqueID of this project. Leave blank to generate one"
)
parser.add_argument(
    '--publisher', '-p', type=str, default=os.getlogin(), help="Name to use as publisher. Defaults to current OS user"
)
parser.add_argument(
    '--openmuze','-om', action='store_true', default=False, help="Also set up an openmuze build pass to combine multiple LUA files into one final index.lua"
)
parser.add_argument(
    '--merge','-m', action='store_true', default=False, help="If the destination directory already exists, merge it with the new structure instead of raising an error. May overwrite some files!"
)

def make_where(use_openmuze=False):
    out  = "\n"
    out += "\n Project code:"
    if use_openmuze:
        out += "\n  - ./source/code/               : Fill with various lua files & {include()} them in main.lua"
        out += "\n  - ./source/code/main.lua       : Replace with your openmuze-compatible LUA code. Will get combined to `romfs/index.lua`"
    else:
        out += "\n  - ./source/romfs/index.lua     : Replace with your LUA code"
    out += "\n"
    out += "\n In-game assets:"
    out += "\n  - ./source/romfs/misc/         : Fill with your uncategorised assets"
    out += "\n  - ./source/romfs/audio/        : Fill with your audio assets"
    out += "\n  - ./source/romfs/models/       : Fill with your 2D assets"
    out += "\n  - ./source/romfs/textures/     : Fill with your 3D assets"
    out += "\n"
    out += "\n 3DS application metadata:"
    out += "\n  - ./source/metadata/icon.png   : Replace with your 3DS home screen icon (must be 48x48)"
    out += "\n  - ./source/metadata/tune.ogg   : Replace with your 3DS home screen selection tune (must be max 3 seconds)"
    out += "\n  - ./source/metadata/banner.png : Replace with your 3DS home screen banner (must be 256x128)"
    out += "\n"
    out += "\n Buildscripts & configuration:"
    out += "\n  - ./build_debug.sh             : Run this to compile a debug build, which gets output to ./outputs"
    out += "\n  - ./build_release.sh           : Run this to compile a release build, which gets output to ./outputs"
    if use_openmuze:    
        out += "\n  - ./conf.sh                    : Contains details such as publisher, project name, & unique ID. Also contains the openmuze compile call"
    else:
        out += "\n  - ./conf.sh                    : Contains details such as publisher, project name, & unique ID"
    if use_openmuze:
        out += "\n"
        out += "\n OpenMuze outputs:"
        out += "\n  - ./source/romfs/index.lua     : This is where the openmuze files will be combined into."
        out += "\n                                   /!\\ Dont make changes to this, they will get overwritten next build /!\\"
    out += "\n"
    out += "\n Build outputs:"
    out += "\n  - ./outputs/romfs              : Output binary location of the generated ROMFS"
    out += "\n  - ./outputs/smdh.bin           : Output binary location of the application icon & metadata"
    out += "\n  - ./outputs/banner.bin         : Output binary location of the application banner"
    out += "\n  - ./outputs/debug_build.3ds    : This is where your playable debug build will get written to (.3ds)"
    out += "\n  - ./outputs/debug_build.cia    : This is where your playable debug build will get written to (.cia)"
    out += "\n  - ./outputs/release_build.cia  : This is where your playable release build will get written to (.cia)"
    out += "\n  - ./outputs/release_build.cia  : This is where your playable release build will get written to (.cia)"

    maxline = 0
    for i in out.split('\n'):
        if len(i) > maxline:
            maxline = len(i)
    out = ' Where to put what? '.center(maxline,'=') + out
    return out

def write_if_notexist(path, contents):
    path.parent.mkdir(parents=True, exist_ok=True)
    if not path.exists():
        with open(str(path.absolute()), 'w') as f:
            f.write(contents)

def main(project_name, publisher, unique_id, project_path=None, merge=False, use_openmuze=False):
    
    if project_path is None:
        project_path = CWD / project_name.replace('/','__')

    print(f"Creating LPP-3DS project '{project_name}'")
    print(f'  At {project_path.absolute()}')
    print(f'  By {publisher}')
    print(f'  With Unique ID {unique_id}')
    if use_openmuze:
        print(f'  With an openmuze build pass')

    template_path = ROOT / 'template'
    print()
    print("Generating structure...")
    try:
        shutil.copytree( str(template_path.absolute()), str(project_path.absolute()), dirs_exist_ok=merge)
    except FileExistsError as e:
        print(f"ERROR: {e}")
        print(" if you want to merge these directories, supply --merge")
        return -1

    write_if_notexist(project_path / 'source' / 'romfs' / 'index.lua',  EXAMPLE_INDEX_LUA)

    if use_openmuze:
        print("Adding openmuze files...")
        write_if_notexist(project_path / 'source' / 'code' / 'main.lua', EXAMPLE_MAIN_LUA)



    print("Setting up conf...")
    conf = generate_conf(project_name, publisher, unique_id, use_openmuze=use_openmuze)
    conf_path = (project_path / 'conf.sh').absolute()
    with open( str(conf_path), 'w') as f:
        f.write(conf)

    conf_path.chmod(conf_path.stat().st_mode | stat.S_IEXEC)
    

    where = make_where(use_openmuze=use_openmuze)
    readme = f"""
{project_name} By {publisher}
UniqueID: {unique_id}
Generated with lpp3-setup {__VERSION__}
({__SOURCE__})

{"This project requires openmuze to build correctly" if use_openmuze else ""}


{where}
"""
    with open( str((project_path / 'readme.txt').absolute()), 'w') as f:
        f.write(readme)
    print("Done!")
    print()
    print(where)
    return 0

def generate_conf(project_name, publisher, unique_id, use_openmuze=False):
    replacers = {
        'VERSION':__VERSION__,
        'SOURCE':__SOURCE__,

        'PROJECT_NAME':project_name,
        'PROJECT_PUBLISHER':publisher,
        'PROJECT_ID':unique_id
    }
    
    out = CONF_TEMPLATE

    for key,value in replacers.items():
        out = out.replace('<<<'+key.upper()+'>>>', value)
    
    if use_openmuze:
        out += OPENMUZE_CONF_ADDITIONS
    
    return out

def cli(args=None):
    if args is None:
        args = sys.argv[1:]
    pargs = parser.parse_args(args)
    sys.exit(main(pargs.name, pargs.publisher, pargs.unique_id, project_path=pargs.destination, merge=pargs.merge, use_openmuze=pargs.openmuze))


if __name__ == '__main__':
    cli(sys.argv[1:])