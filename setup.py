import os
from setuptools import setup,find_packages

import lpp3setup


requirements = ""
with open('./requirements.txt') as f:
	requirements = f.read()

license = ""
with open('./LICENSE') as f:
	license = f.read()

readme = ""
with open('./README.md') as f:
	readme = f.read()

setup(
	name = 'lpp3-setup',
	description = "a utility for setting up Luna Player Plus 3DS projects that use `lpp3-pack` quickly",
	long_description = readme,
	long_description_content_type = 'text/markdown',
	version = str(lpp3setup.__VERSION__),
	url = str(lpp3setup.__SOURCE__),
	license = license,

	packages = [ 'lpp3setup' ],
	include_package_data = True,

	entry_points = {
		'console_scripts': [
			'lpp3-setup = lpp3setup:cli',
			]
	},

	install_requires = requirements.split('\n'),
	
	python_requires = '>=3.10',
	classifiers = [
		"Programming Language :: Python :: 3",
		"Operating System :: OS Independent",
		"Environment :: Console",
		"License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
		"Topic :: Software Development",
	]
)