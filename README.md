# lpp3-setup
Quick project structure setup tool for ([Luna Player Plus 3DS](https://github.com/rinnegatamante/lpp-3ds)) 3DS homebrew using [lpp3-pack](https://gitlab.com/mocchapi/lpp3-pack), [lpp3-check](https://gitlab.com/mocchapi/lpp3-check), & optionally [openmuze]()

The tool generates a copy of the file structure as seen in the `template` directory, & generates additional placeholder files and a `conf.sh` file for storing the project parameters  
The `build_debug.sh` & `build_release.sh` scripts let you easily bundle, check, & compile your project to a .CIA and/or .3DS file.  

## Installation
Python 3.10 is required.  
```bash
pip install git+https://gitlab.com/mocchapi/lpp3-setup.git
```
alternatively, if you have already cloned this repo locally:
```bash
pip install .
```
or:
```bash
python setup.py install
```
This will install the `lpp3-setup` terminal application into your path.  


## Usage
Use the `lpp3-setup.py` python program.  
### Basic example:
Generate a project named `my_cool_project` with no openmuze build step
```bash
lpp3-setup my_cool_project
```
Generate a project named `my_other_project` with the publisher set as `among us` & with an openmuze build step
```bash
lpp3-setup my_other_project --publisher "among us" --openmuze
```
Add to/update an existing LPP-3DS project:  
(Note: the 0x1234 should match your existing projects ID as given to lpp3-pack)
```bash
lpp3-setup "My existing project name" --publisher existing_project_publisher --unique-id 0x1234 --destination ./existing_project_directory --merge 
```

### All parameters:  
```
usage: lpp3-setup [-h] [--destination DESTINATION] [--unique-id UNIQUE_ID]
                  [--publisher PUBLISHER] [--openmuze] [--merge]
                  name

a utility for setting up Luna Player Plus 3ds projects that use `lpp3-pack`
quickly

positional arguments:
  name                  Name of the project

options:
  -h, --help            show this help message and exit
  --destination DESTINATION, -d DESTINATION
                        Path to create the project at. Defaults to `./<project
                        name>` (default: None)
  --unique-id UNIQUE_ID, -id UNIQUE_ID
                        The UniqueID of this project. Leave blank to generate
                        one (default: 0x21705)
  --publisher PUBLISHER, -p PUBLISHER
                        Name to use as publisher. Defaults to current OS user
                        (default: anne)
  --openmuze, -om       Also set up an openmuze build pass to combine multiple
                        LUA files into one final index.lua (default: False)
  --merge, -m           If the destination directory already exists, merge it
                        with the new structure instead of raising an error.
                        May overwrite some files! (default: False)

this software is licensed under the AGPLv3.0, for more info see the LICENSE
file
```
